FROM ubuntu:18.04

RUN apt-get update -q && apt-get install -y -q build-essential gcc-multilib g++-multilib curl
RUN curl -L https://github.com/Kitware/CMake/releases/download/v3.22.0-rc2/cmake-3.22.0-rc2-linux-x86_64.sh -o cmake.sh
RUN chmod +x cmake.sh && ./cmake.sh --prefix=/usr/local --exclude-subdir && rm cmake.sh
